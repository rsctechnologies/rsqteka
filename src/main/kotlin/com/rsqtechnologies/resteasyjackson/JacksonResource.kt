package com.rsqtechnologies.resteasyjackson

import java.util.*
import javax.annotation.security.RolesAllowed
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.SecurityContext

@Path("/resteasy-jackson/quarks")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RolesAllowed(value = ["rsq_user", "rsq_admin"])
class JacksonResource {

    private val quarks = Collections.newSetFromMap(Collections.synchronizedMap(LinkedHashMap<Quark, Boolean>()))

    init {
        quarks.add(Quark("Up", "The up quark or u quark (symbol: u) is the lightest of all quarks, a type of elementary particle, and a major constituent of matter."))
        quarks.add(Quark("Strange", "The strange quark or s quark (from its symbol, s) is the third lightest of all quarks, a type of elementary particle."))
        quarks.add(Quark("Charm", "The charm quark, charmed quark or c quark (from its symbol, c) is the third most massive of all quarks, a type of elementary particle."))
        quarks.add(Quark("???", null))
    }

    @GET
    fun list(@Context securityContext: SecurityContext): Set<Quark> {
        println(securityContext.userPrincipal.name)
        return quarks
    }

    @POST
    fun add(quark: Quark): Set<Quark> {
        quarks.add(quark)
        return quarks
    }

    @DELETE
    fun delete(quark: Quark): Set<Quark> {
        quarks.removeIf { existingQuark: Quark -> existingQuark.name!!.contentEquals(quark.name!!) }
        return quarks
    }

    class Quark(var name: String?, var description: String?)
}
