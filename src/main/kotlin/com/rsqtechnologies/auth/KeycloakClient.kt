package com.rsqtechnologies.auth

import org.jboss.resteasy.client.jaxrs.internal.ResteasyClientBuilderImpl
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.admin.client.resource.RealmResource
import org.keycloak.admin.client.resource.UserResource
import org.keycloak.admin.client.resource.UsersResource
import org.keycloak.representations.idm.CredentialRepresentation
import org.keycloak.representations.idm.UserRepresentation
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.Response

@ApplicationScoped
class KeycloakClient(
    keycloakAdminConfig: KeycloakAdminConfig
) {
    companion object {
        // TODO variable in properties file
        const val SERVER_URL = "http://localhost:9080/auth"
        const val MASTER_REALM = "master"
    }

    private val client = KeycloakBuilder.builder()
        .serverUrl(SERVER_URL)
        .realm(MASTER_REALM)
        .username(keycloakAdminConfig.username)
        .password(keycloakAdminConfig.password)
        .clientId(keycloakAdminConfig.clientId)
        .clientSecret(keycloakAdminConfig.clientSecret)
        .resteasyClient(ResteasyClientBuilderImpl().connectionPoolSize(10).build())
        .build()

    fun createUser(userRepresentation: UserRepresentation): Response =
        getUsersResource().create(userRepresentation)

    fun setUserPassword(userId: String, password: String) {
        val passwordCredential = createPasswordCredential(password)
        getUserResource(userId).resetPassword(passwordCredential)
    }

    fun addUserRole(userId: String, userRole: String) {
        val userRoleRepresentation = getMasterRealmResource().roles().get(userRole).toRepresentation()
        getUserResource(userId).roles().realmLevel().add(listOf(userRoleRepresentation))
    }

    private fun getMasterRealmResource(): RealmResource =
        client.realm(MASTER_REALM)

    private fun getUsersResource(): UsersResource =
        getMasterRealmResource().users()

    private fun getUserResource(id: String): UserResource =
        getUsersResource().get(id)

    private fun createPasswordCredential(password: String) =
        CredentialRepresentation().apply {
            isTemporary = false
            type = CredentialRepresentation.PASSWORD
            value = password
        }
}