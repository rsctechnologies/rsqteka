package com.rsqtechnologies.auth

import com.rsqtechnologies.auth.dto.UserDTO
import javax.annotation.security.PermitAll
import javax.json.bind.JsonbBuilder
import javax.validation.Valid
import javax.validation.ValidationException
import javax.validation.Validator
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.core.Response

@Path("/users")
@PermitAll
class KeycloakController(
    private val keycloakService: KeycloakService,
    private val validator: Validator
) {
    @POST
    fun createUser(@Valid userDTO: UserDTO): Response {
        val violations = validator.validate(userDTO)
        if (violations.isNotEmpty()) {
            throw ValidationException(JsonbBuilder.create().toJson(violations))
        }
        return keycloakService.createUser(userDTO)
    }
}