package com.rsqtechnologies.auth.dto

import com.rsqtechnologies.auth.validation.Password
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class UserDTO(
    @field:Email
    @field:Size(max = 64)
    val email: String,

    @field:Password
    val password: String,

    @field:NotBlank
    val firstName: String,

    @field:NotBlank
    val lastName: String
)