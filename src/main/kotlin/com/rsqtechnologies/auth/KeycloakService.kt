package com.rsqtechnologies.auth

import com.rsqtechnologies.auth.dto.UserDTO
import org.keycloak.representations.idm.UserRepresentation
import javax.inject.Singleton
import javax.ws.rs.core.Response

@Singleton
class KeycloakService(
    private val keycloakClient: KeycloakClient
) {
    companion object {
        const val USER_ROLE = "rsq_user"
    }

    fun createUser(userDTO: UserDTO): Response {
        val userRepresentation = userDTO.toUserRepresentation()
        val keycloakResponse = keycloakClient.createUser(userRepresentation)

        if (keycloakResponse.statusInfo.toEnum() == Response.Status.CREATED) {
            val userId = keycloakResponse.location.path.replace(".*/([^/]+)$".toRegex(), "$1")
            keycloakClient.setUserPassword(userId, userDTO.password)
            keycloakClient.addUserRole(userId, USER_ROLE)
        }

        return keycloakResponse
    }
}

private fun UserDTO.toUserRepresentation() = UserRepresentation().apply {
    email = this@toUserRepresentation.email
    username = this@toUserRepresentation.email
    firstName = this@toUserRepresentation.firstName
    lastName = this@toUserRepresentation.lastName
    isEnabled = true
    isEmailVerified = true
}