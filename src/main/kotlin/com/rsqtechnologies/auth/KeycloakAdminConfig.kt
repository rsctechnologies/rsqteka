package com.rsqtechnologies.auth

import io.quarkus.arc.config.ConfigProperties

@ConfigProperties(prefix = "keycloak.admin")
class KeycloakAdminConfig {
    lateinit var username: String
    lateinit var password: String
    lateinit var clientId: String
    lateinit var clientSecret: String
}