package com.rsqtechnologies.bookset.model

import com.rsqtechnologies.bookset.dto.BookDTO
import com.rsqtechnologies.bookset.dto.toCategoryDTO
import io.quarkus.hibernate.orm.panache.kotlin.PanacheEntityBase
import org.hibernate.annotations.GenericGenerator
import javax.persistence.Column
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class BookEntity : PanacheEntityBase {
//    companion object : PanacheCompanion<BookEntity> {
//        fun findALl() = listAll()
//    }

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column
    lateinit var id: String

    @Column
    lateinit var title: String

    @ElementCollection
    @Column
    lateinit var authors: List<String>

    @Column
    lateinit var language: String

    @Column
    lateinit var datePublished: String

    @Column
    lateinit var description: String

    @Enumerated(EnumType.STRING)
    @Column
    lateinit var category: Category

    @Column
    lateinit var publisher: String

    @Column
    lateinit var isbn: String

    fun toBookDTO(): BookDTO = BookDTO(
        title = title,
        authors = authors,
        language = language,
        datePublished = datePublished,
        description = description,
        category = category.toCategoryDTO(),
        publisher = publisher,
        ISBN = isbn
    )

    enum class Category {
        NONE,
        PROGRAMMING,
        IT,
        LAW,
        MARKETING,
        MANAGEMENT,
        BUSINESS
    }
}
