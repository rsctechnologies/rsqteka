package com.rsqtechnologies.bookset.model

import com.rsqtechnologies.library.model.BookInstance
import com.rsqtechnologies.library.repository.BookRepository
import com.rsqtechnologies.library.service.BookResponse
import com.rsqtechnologies.library.service.IsbnBookResponse
import org.eclipse.microprofile.config.inject.ConfigProperty
import java.time.LocalDate
import java.util.Collections.emptyList
import java.util.concurrent.Executors
import javax.enterprise.context.RequestScoped
import javax.inject.Inject
import javax.transaction.Transactional
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.QueryParam
import javax.ws.rs.client.ClientBuilder

@Path("/external")
@RequestScoped
class ExternalBookInfoResource(
    @Inject
    val bookEntityRepository: BookEntityRepository,
    @Inject
    val bookRepository: BookRepository
) {
    private val executorService = Executors.newCachedThreadPool()
    private val client = ClientBuilder.newBuilder()
        .executorService(executorService)
        .build()

    @ConfigProperty(name = "library.external.isbndb.apikey")
    lateinit var apikey: String

    private fun getExternalBookInfo(isbn: String): IsbnBookResponse? {
        val thePath = "https://api2.isbndb.com/book/$isbn"
        client.target(thePath)
            .request()
            .header("Authorization", apikey)
            .get().use { getPostByIdResponse ->
                return getPostByIdResponse.readEntity(IsbnBookResponse::class.java)
            }
    }

    @Transactional
    @POST
    fun addBook(@QueryParam("isbn") isbn: String): BookResponse {
        val book = getExternalBookInfo(isbn)?.book
                ?: throw Exception("Book with isbn $isbn not found")         //TODO: Throw NOT_FOUND
        (bookEntityRepository.findByIsbn(isbn) ?: BookEntity()
            .apply {
                title = book.title ?: ""
                authors = book.authors ?: emptyList()
                language = ""
                datePublished = book.date_published ?: ""
                description = "FAJNIE JEST"
                category = BookEntity.Category.PROGRAMMING
                publisher = book.publisher ?: ""
                this.isbn = isbn
            }
            .also { bookEntityRepository.persist(it) })
            .also {
                bookRepository.persist(
                    BookInstance()
                        .apply {
                            bookEntity = it
                            owner = "RSQ" //TODO add to BookingDTO?
                        },

                    )
            }.toBookDTO()
        return book
    }
}
