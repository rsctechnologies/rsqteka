package com.rsqtechnologies.bookset.model

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepositoryBase
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class BookEntityRepository : PanacheRepositoryBase<BookEntity, Long> {
    fun findByIsbn(isbn: String): BookEntity? {
        return find("isbn", isbn).firstResult()
    }
}