package com.rsqtechnologies.bookset.dto

data class BookDTO(
    val title: String,
    val authors: List<String>,
    val language: String,
    val datePublished: String,
    val description: String,
    val category: CategoryDTO,
    val publisher: String,
    val ISBN: String
)