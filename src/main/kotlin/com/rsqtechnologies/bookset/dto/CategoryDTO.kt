package com.rsqtechnologies.bookset.dto

import com.rsqtechnologies.bookset.model.BookEntity

enum class CategoryDTO {
    NONE,
    PROGRAMMING,
    IT,
    LAW,
    MARKETING,
    MANAGEMENT,
    BUSINESS
}

fun CategoryDTO.toCategory(): BookEntity.Category = when (this) {
    CategoryDTO.NONE -> BookEntity.Category.NONE
    CategoryDTO.PROGRAMMING -> BookEntity.Category.PROGRAMMING
    CategoryDTO.IT -> BookEntity.Category.IT
    CategoryDTO.LAW -> BookEntity.Category.LAW
    CategoryDTO.MARKETING -> BookEntity.Category.MARKETING
    CategoryDTO.MANAGEMENT -> BookEntity.Category.MANAGEMENT
    CategoryDTO.BUSINESS -> BookEntity.Category.BUSINESS
}

fun BookEntity.Category.toCategoryDTO(): CategoryDTO = when (this) {
    BookEntity.Category.NONE -> CategoryDTO.NONE
    BookEntity.Category.PROGRAMMING -> CategoryDTO.PROGRAMMING
    BookEntity.Category.IT -> CategoryDTO.IT
    BookEntity.Category.LAW -> CategoryDTO.LAW
    BookEntity.Category.MARKETING -> CategoryDTO.MARKETING
    BookEntity.Category.MANAGEMENT -> CategoryDTO.MANAGEMENT
    BookEntity.Category.BUSINESS -> CategoryDTO.BUSINESS
}