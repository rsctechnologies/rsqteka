package com.rsqtechnologies.library.model

import com.rsqtechnologies.bookset.model.BookEntity
import io.quarkus.hibernate.orm.panache.kotlin.PanacheEntityBase
import org.hibernate.annotations.GenericGenerator
import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class BookInstance : PanacheEntityBase {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column
    lateinit var id: String

    @ManyToOne(fetch = FetchType.EAGER)
    lateinit var bookEntity: BookEntity

    @Column
    lateinit var owner: String

//    @Column
//    lateinit var editionNumber: IntegerType
}