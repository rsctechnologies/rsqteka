package com.rsqtechnologies.library.service

data class IsbnBookResponse(
    val book: BookResponse? = null
)

data class BookResponse(
    val publisher: String?,
    val image: String?,
    val title_long: String?,
    val pages: Int?,
    val date_published: String?,
    val authors: List<String>?,
    val title: String?,
    val isbn13: String?,
    val msrp: String?,
    val binding: String?,
    val isbn: String?
)
