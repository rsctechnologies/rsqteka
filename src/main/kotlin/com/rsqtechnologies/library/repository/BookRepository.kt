package com.rsqtechnologies.library.repository

import com.rsqtechnologies.library.model.BookInstance
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepositoryBase
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class BookRepository : PanacheRepositoryBase<BookInstance, Long>