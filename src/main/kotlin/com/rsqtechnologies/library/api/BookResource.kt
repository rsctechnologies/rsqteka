package com.rsqtechnologies.library.api

import com.rsqtechnologies.library.model.BookInstance
import com.rsqtechnologies.library.repository.BookRepository
import org.eclipse.microprofile.graphql.Description
import org.eclipse.microprofile.graphql.Mutation
import javax.inject.Inject
import javax.ws.rs.DELETE
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.QueryParam

@Path("/books")
class BookResource(
    @Inject
    var bookRepository: BookRepository
) {

    @GET
    fun allBooks(): List<BookInstance> = bookRepository.listAll()

    @Mutation("updateBook")
    @Description("Add a Book")
    fun updateBook(book: BookInstance): String {
//        bookRepository.update(book)
        return "OK"
    }

    @POST
    fun addBook(book: BookInstance): String {
        bookRepository.persist(book)
        return "CREATED"
    }

    @DELETE
    fun deleteBook(@QueryParam("id") id: Long): String {
        val book: BookInstance? = bookRepository.findById(id)
        book?.let { bookRepository.delete(it) }
        return "NO_CONTENT"
    }
}